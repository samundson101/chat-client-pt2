/*
Chat client Project pt 2
Sam Amundson
i'm pretty sure this is a good and fully functional final working of this project
Worked a bit with Maya
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <time.h>
#include <ncurses.h>

/*
SERVER = address of the server the program connects to
PORT  = port program connects to on the server
BUFSIZE = size of the buffer being used
*/
#define SERVER "10.115.20.250"
#define PORT 49153
#define BUFSIZE 100
#define HI1 3
#define HI2 20
#define WI 75


//sending information to the server and dealing with errors
int sendout(WINDOW * swin, WINDOW * rwin, int fd, char *msg){
  int ret;
  ret = send(fd,msg,strlen(msg),0);
  if(ret == -1){
    wscrl(rwin, 1);
    mvwprintw(rwin,HI2-1,1,"ERROR: trouble sending. errno = %d", errno);
    wrefresh(rwin);
    exit(errno);
  }
  if(ret>0){
    wscrl(rwin, 1);
    wrefresh(rwin);
  }
  return strlen(msg);
}

/*
building the string to send to the server
*/
char* addtomessage(WINDOW * swin, WINDOW * rwin,int fd,char* user ,char * msg, char add, int * curs){
  int val = (int) add;
  //if left empty don't mess with string
  if (val ==-1){
    return msg;
  }
  //backspace removing the latest character
  else if (val == 127){
    msg[*curs-1] = '\0';
    if(*curs >0){
      *curs = *curs-1;
    }
    wclear(swin);
    box(swin,0,0);
    wrefresh(rwin);
    wrefresh(swin);
    mvwprintw(swin,1,1,"%s",msg);
    wrefresh(swin);
    return msg;
  }
  //sending message, enter key
  else if (val == 10){

    msg[*curs] = add;
    sendout(swin,rwin,fd,msg);

    wscrl(rwin, 1);
    /*
    testing to make sure messages worked properly, just prints out a message before sending it to the server
    mvwprintw(rwin,HI2-1,1,"%s",user);
    wscrl(rwin,1);
    mvwprintw(rwin,HI2-1,4,"%s\n",msg);
    */
    wclear(swin);
    box(swin,0,0);
    wrefresh(rwin);
    wrefresh(swin);
    memset(msg,0,*curs);
    *curs =0;


    return msg;
  }
  //add a value if it is a non-special character
  else if (val > 31){
    msg[*curs] = add;
    mvwprintw(swin,1,1,"%s",msg);
    wrefresh(swin);

    *curs = *curs +1;
    return msg;
  }
  //don't know what was added
  else{
    wscrl(rwin, 1);
    mvwprintw(rwin,HI2-1,1,"unable to add value to string");
    wrefresh(rwin);
    return msg;
  }
}

/*
recieving and printing information from the server until nothing left to recieve
and dealing with errors
*/
void recvandprint(WINDOW * swin, WINDOW * rwin, int fd, char *buff ){
  int ret;
  for(;;){
    buff = malloc(BUFSIZE+1);
    ret = recv(fd,buff,BUFSIZE,0);
    if(ret == -1){
      if(errno == EAGAIN){
        break;
      } else {
        wscrl(rwin, 1);
        mvwprintw(rwin,HI2-1,1,"ERROR: error recieving. errno = %d", errno);
        wrefresh(rwin);
        exit(errno);
      }
    }

    else if (ret ==0){
      //mvwprintw(rwin,HI2-1,1,"recieved nothing");
      exit(0);
    }

    else {
      wscrl(rwin, 1);
      buff[ret] = 0;
      mvwprintw(rwin,HI2-1,1,"%s",buff);
      wrefresh(rwin);
    }
    free(buff);
  }
}

/*
establishing the connection to the server and port
return a file descriptor
*/
int connect2v4stream(WINDOW * rwin, WINDOW * swin, char * srv,int port){
  int ret,sockd;
  struct sockaddr_in sin;
  wscrl(rwin, 1);
  //mvwprintw(rwin,HI2-1,1,"starting connection process");
  wrefresh(rwin);
  if( (sockd = socket(AF_INET, SOCK_STREAM, 0)) == -1){
    wscrl(rwin, 1);
    mvwprintw(rwin,HI2-1,1,"ERROR: Error creating socket. errno = %d",errno);
    wrefresh(rwin);
    exit(errno);
  }

  if( (ret = inet_pton(AF_INET, SERVER, &sin.sin_addr)) <= 0){
    wscrl(rwin, 1);
    mvwprintw(rwin,HI2-1,1,"ERROR: trouble converting using inet_pton.  return value = %d, errno = %d", ret, errno);
    wrefresh(rwin);
    exit(errno);
  }

  sin.sin_family = AF_INET;//IPV4
  sin.sin_port = htons(PORT); //convert port to network endian

  if((connect(sockd, (struct sockaddr *) &sin, sizeof(sin))) == -1){
    wscrl(rwin, 1);
    mvwprintw(rwin,HI2-1,1,"ERROR: trouble connecting to server. errno = %d", errno);
    wrefresh(rwin);
    exit(errno);
  }
  wscrl(rwin, 1);
  //mvwprintw(rwin,HI2-1,1,"should be connected properly");
  wrefresh(rwin);
  return sockd;
}

//Main Function
int main(int argc, char *argv[]){

  int fd, len;
  char *name, *buffer, *buff;
  struct timeval timev;

  //Initializes the screen and does some housekeeping for it
  initscr();
  cbreak();
  noecho();


  int sty, stx;

  sty = stx = 0;
  //making the windows
  WINDOW  * rwin = newwin(HI2,WI,sty,stx);
  WINDOW * swin = newwin(HI1, WI, sty+HI2,stx);
  refresh();
  //setup permissions for the windows
  scrollok(rwin,1);
  idlok(rwin,1);
  keypad(swin,1);
  wtimeout(swin,500);
  box(swin,0,0);
  refresh();

  char rec;
  int cur = 0;
  wscrl(rwin,1);

  fd = connect2v4stream(rwin, swin, SERVER, PORT );
  buff = malloc(BUFSIZE+1);

  wscrl(rwin,1);
  //Setting up the recv timeout for < 0.5 seconds, if its up to 0.5 it starts to feel robotic and slow
  timev.tv_sec=0;
  timev.tv_usec=100*500;
  setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, &timev, sizeof(timev));

  //seting name based on arguments
  if(argc<2){
    wscrl(rwin, 1);
    mvwprintw(rwin,HI2-1,1,"Usage: chat-client <screename>");
    wrefresh(rwin);
    exit(1);
  }
  name = argv[1];
  len = strlen(name);
  name[len] = '\n';
  name[len+1] ='\0';
  sendout(rwin, swin, fd,name);
  memset(buff,0,BUFSIZE);
  //THE LOOP!
  while( 1 ){
    recvandprint(swin,rwin,fd,buffer);
    rec = wgetch(swin);
    addtomessage(swin,rwin,fd,name,buff,rec,&cur);
    memset(buffer,'\0',sizeof(buffer));
    wrefresh(rwin);
    wrefresh(swin);
  }

  endwin();
}
