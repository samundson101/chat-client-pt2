#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>

int main(int argc, char *argv[]){
  //initializes the screen
  initscr();
  cbreak();
  noecho();
  int c=1;
  int hi1, hi2, wi, sty, stx;
  hi1 = 3;
  hi2 = 20;
  wi = 65;
  stx = sty = 0;
  WINDOW  * win = newwin(hi1,wi,sty+hi2,stx);
  WINDOW * win2 = newwin(hi2, wi, sty,stx);
  refresh();
// box(win2,0,0);
  box(win,0,0);
  wrefresh(win);
  wrefresh(win2);
  //  while(1){
  //c = getch();
  wtimeout(win2, 1000);
  scrollok(win2, 1);
  idlok(win2, 1);
  mvwprintw(win,1,1,"this is my box");
  wrefresh(win);
//  mvwprintw(win2,1,1,"my box is amazing");
  wrefresh(win2);

  while(1){
    mvwprintw(win2,hi2-1,1,"%d",wgetch(win2));
    mvwprintw(win2,hi2-1,15,"%d",c);
    wscrl(win2, 1);
    c++;
    wrefresh(win2);
  }

  getch();
  //printw("hello world");
  //refresh();
  //getch();
  endwin();
  return 0;
}
